package com.app.profiles.domain.entity;

import java.util.Calendar;
import java.util.Date;

public class User {
    private int idUser;
    private String name;
    private String lastName;
    private String email;
    private int experience;
    private int age;
    private String tecnologies;
    private boolean transfer;
    private Calendar birthDate;
    private int idProfile;
    private String country;
    private String city;

    public User() {
    }

    public User(int idUser, String name, String lastName, String email, int experience, int age, String tecnologies, boolean transfer, Calendar birthDate, int idProfile, String country, String city) {
        this.idUser = idUser;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.experience = experience;
        this.age = age;
        this.tecnologies = tecnologies;
        this.transfer = transfer;
        this.birthDate = birthDate;
        this.idProfile = idProfile;
        this.country = country;
        this.city = city;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTecnologies() {
        return tecnologies;
    }

    public void setTecnologies(String tecnologies) {
        this.tecnologies = tecnologies;
    }

    public boolean isTransfer() {
        return transfer;
    }

    public void setTransfer(boolean transfer) {
        this.transfer = transfer;
    }

    public Calendar getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Calendar birthDate) {
        this.birthDate = birthDate;
    }

    public int getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(int idProfile) {
        this.idProfile = idProfile;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "User{" +
                "idUser=" + idUser +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", experience=" + experience +
                ", age=" + age +
                ", tecnologies='" + tecnologies + '\'' +
                ", transfer=" + transfer +
                ", birthDate=" + birthDate +
                ", idProfile=" + idProfile +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
