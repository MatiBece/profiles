package com.app.profiles.domain.entity;

import java.util.Date;

public class Experience {
    private int idExperience;
    private Date startDate;
    private Date endDate;
    private String type;
    private String company;
    private int idProfile;

    public Experience() {
    }

    public Experience(int idExperience, Date startDate, Date endDate, String type, String company, int idProfile) {
        this.idExperience = idExperience;
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
        this.company = company;
        this.idProfile = idProfile;
    }

    public int getIdExperience() {
        return idExperience;
    }

    public void setIdExperience(int idExperience) {
        this.idExperience = idExperience;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(int idProfile) {
        this.idProfile = idProfile;
    }

    @Override
    public String toString() {
        return "Experience{" +
                "idExperience=" + idExperience +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", type='" + type + '\'' +
                ", company='" + company + '\'' +
                ", idProfile=" + idProfile +
                '}';
    }
}
