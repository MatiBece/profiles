package com.app.profiles.domain.entity;

import java.util.Date;

public class Academics {
    private int idAcademics;
    private Date startDate;
    private Date endDate;
    private String type;
    private String university;
    private int idProfile;
    private boolean finished;

    public Academics() {
    }

    public Academics(int idAcademics, Date startDate, Date endDate, String type, String university, int idProfile, boolean finished) {
        this.idAcademics = idAcademics;
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
        this.university = university;
        this.idProfile = idProfile;
        this.finished = finished;
    }

    public int getIdAcademics() {
        return idAcademics;
    }

    public void setIdAcademics(int idAcademics) {
        this.idAcademics = idAcademics;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public int getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(int idProfile) {
        this.idProfile = idProfile;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    @Override
    public String toString() {
        return "Academics{" +
                "idAcademics=" + idAcademics +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", type='" + type + '\'' +
                ", university='" + university + '\'' +
                ", idProfile=" + idProfile +
                ", finished=" + finished +
                '}';
    }
}
