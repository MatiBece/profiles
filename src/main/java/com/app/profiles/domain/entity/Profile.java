package com.app.profiles.domain.entity;

import java.util.List;

public class Profile {
    private int idProfile;
    private String document;
    private User user;
    private List<Experience> experience;
    private List<Academics> academics;

    public Profile(int idProfile, String document, User user, List<Experience> experience, List<Academics> academics) {
        this.idProfile = idProfile;
        this.document = document;
        this.user = user;
        this.experience = experience;
        this.academics = academics;
    }

    public Profile() {
    }

    public int getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(int idProfile) {
        this.idProfile = idProfile;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Experience> getExperience() {
        return experience;
    }

    public void setExperience(List<Experience> experience) {
        this.experience = experience;
    }

    public List<Academics> getAcademics() {
        return academics;
    }

    public void setAcademics(List<Academics> academics) {
        this.academics = academics;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "idProfile=" + idProfile +
                ", document='" + document + '\'' +
                ", user=" + user +
                ", experience=" + experience +
                ", academics=" + academics +
                '}';
    }
}
