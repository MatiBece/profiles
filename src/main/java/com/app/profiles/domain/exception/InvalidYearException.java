package com.app.profiles.domain.exception;

public class InvalidYearException extends RuntimeException {
    public InvalidYearException () {
        super("No puede ingresar un año de nacimiento futuro");
    }
}
