package com.app.profiles.domain.exception;

public class InvalidEmailException extends RuntimeException{
    public InvalidEmailException() {
        super("Email ingresado no es valido");
    }

    public static boolean validateEmail(String email) {
        String regex = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
        return email.matches(regex);
    }
}
