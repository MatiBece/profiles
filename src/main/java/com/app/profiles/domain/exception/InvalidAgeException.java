package com.app.profiles.domain.exception;


import java.time.LocalDate;

public class InvalidAgeException extends RuntimeException {
    public InvalidAgeException(LocalDate localDate) {

        super("Debe ingresar una edad mayor a 18");

    }
}
