package com.app.profiles.domain.service;

import com.app.profiles.domain.entity.Academics;
import com.app.profiles.domain.entity.Profile;
import com.app.profiles.persistence.repository.AcademicsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AcademicsService {

    @Autowired
    private AcademicsRepository repository;

    public List<Academics> getAll(String type, String type2) throws Exception {
        //List<Academics> academicsList = repository.getAll();
        //if(academicsList.isEmpty()) {
        //    throw new Exception("No hay datos academicos");
        //}
        //return academicsList;

        System.out.println(type2);
        List<Academics> academicsList = new ArrayList<Academics>();
        if (type == null) {
            repository.getAll().forEach(academicsList::add);
        }else {
            repository.getByType(type, type2).forEach(academicsList::add);
        }
        return academicsList;
    }

    public Academics getById(int id) throws Exception {
        Optional<Academics> academics = repository.getById(id);
        if (academics.isPresent()){
            return academics.get();
        }
        throw new Exception("No hay datos academicos con ese id");
    }

    public Academics saveAcademics(Academics academics) throws Exception {
        //if(repository.getById(academics.getIdAcademics()).isPresent()){
        //    throw new Exception("Ya existen datos academicos cono ese ID");
        //}

        academics = repository.saveAcademic(academics);
        return academics;
    }

    public Optional<Academics> updateAcademics(Academics academics, int id) throws Exception {
        Optional<Academics> academicsData = repository.getById(id);
        if(academicsData.isPresent()){
            Academics newAcademics = academicsData.get();
            newAcademics.setStartDate(academics.getStartDate());
            newAcademics.setEndDate(academics.getEndDate());
            newAcademics.setType(academics.getType());
            newAcademics.setUniversity(academics.getUniversity());
            newAcademics.setFinished(academics.isFinished());
            Optional<Academics> returnAca = repository.updateAcademics(newAcademics);
            return  returnAca;
        }
        throw new Exception("No existen datos academicos con ese id");
    }

    public void deleteAcademicsById(int id) throws Exception {
        Optional<Academics> academicsQuery = repository.getById(id);
        if(academicsQuery.isPresent()) {
            repository.deleteAcademicsById(id);
        } else {
            throw new Exception("No existen datos academicos con ese id");
        }
    }

}
