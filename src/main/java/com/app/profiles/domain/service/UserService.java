package com.app.profiles.domain.service;

import com.app.profiles.domain.entity.User;
import com.app.profiles.domain.exception.InvalidAgeException;
import com.app.profiles.domain.exception.InvalidEmailException;
import com.app.profiles.domain.exception.InvalidYearException;
import com.app.profiles.infrastructure.crud.UserCrudRepository;
import com.app.profiles.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    LocalDate localDate = LocalDate.now();

    int year = localDate.getYear();

    public List<User> getAll() throws Exception {
        List<User> userList = repository.getAll();
        if(userList.isEmpty()){
            throw new Exception("No hay ningun usuario en la base de datos");
        }
        return userList;
    }

    public User getById(int id) throws Exception {
        Optional<User> user = repository.getById(id);
        if (user.isPresent()){
            return user.get();
        }
        throw new Exception("No existe usuario con ese id");
    }

    public User saveUser(User user) throws InvalidAgeException, InvalidYearException, InvalidEmailException {
        //if(repository.getById(user.getIdUser()).isPresent()){
        //    throw new Exception("Ya existe usuario con ese id");
        //}
        int userYear = user.getBirthDate().get(Calendar.YEAR);

        if(user.getAge() < 18 ) {
            throw new InvalidAgeException(localDate);
        }

        if(year < userYear) {
            System.out.println("YEAR"+year);
            System.out.println("USERYEAR"+userYear);
            throw new InvalidYearException();
        }

        //if(InvalidEmailException.validateEmail(user.getEmail())) {
        //    throw new InvalidEmailException();
        //}

        user = repository.saveUser(user);
        return user;
    }

    // METODO PUT CON EL ID EN PAYLOAD
    /*public User updateUser(User user) throws Exception {
        if(repository.getById(user.getIdUser()).isPresent()) {
            deleteUserById(user.getIdUser());
            user = repository.saveUser(user);
            return user;
        }
        throw new Exception("Usuario no encontrado");
    }*/


    // METODO PUT CON EL ID EN ENDPOINT
    public User updateUser(User user, int id) throws Exception {
        Optional<User> userData = repository.getById(id);
        if(userData.isPresent()){
            User newUser =  userData.get();
            newUser.setName(user.getName());
            newUser.setLastName(user.getLastName());
            newUser.setEmail(user.getEmail());
            newUser.setExperience(user.getExperience());
            newUser.setAge(user.getAge());
            newUser.setTecnologies(user.getTecnologies());
            newUser.setTransfer(user.isTransfer());
            repository.saveUser(newUser);
            return newUser;
        }
        throw new Exception("No existe usuario con este id");
    }

    public void deleteUserById(int id) throws Exception {
        Optional<User> userQuery = repository.getById(id);
        if (userQuery.isPresent()) {
            repository.deleteUserById(id);
        } else {
            throw new Exception("No existe usuario con ese id");
        }
    }

}
