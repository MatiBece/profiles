package com.app.profiles.domain.service;

import com.app.profiles.domain.entity.Experience;
import com.app.profiles.domain.entity.Profile;
import com.app.profiles.persistence.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProfileService {

    @Autowired
    private ProfileRepository repository;

    public List<Profile> getAll(String type, String type2, String country, String city, Boolean transfer, String technologies, Integer exp) throws Exception {

        List<Profile> profileList = new ArrayList<Profile>();

        if (type == null && type2 == null && country == null && city == null && transfer == null && technologies == null && exp == null) {
            repository.getAll().forEach(profileList::add);
        } else if (type == null && type2 == null && country == null && city == null && transfer == null && technologies == null ){
            repository.getByExperienceYear(exp).forEach(profileList::add);
        } else if (type == null && type2 == null && country == null && city == null && transfer == null) {
            repository.getByUsersType(technologies).forEach(profileList::add);
        } else if (country == null && city == null && transfer == null && technologies == null) {
            repository.getByAcademicsType(type, type2).forEach(profileList::add);
        } else if (type == null && type2 == null && technologies == null) {
            repository.getByUsersFilters(country, city, transfer).forEach(profileList::add);
        }

        return profileList;
    }

    public Profile getById(int id) throws Exception {
        Optional<Profile> profile = repository.getById(id);
        if (profile.isPresent()) {
            return profile.get();
        }
        throw new Exception("No existe un perfil con ese id");
    }

    public Profile saveProfile(Profile profile) throws Exception {
        profile = repository.saveProfile(profile);
        return profile;
    }

    public void deleteProfileById(int id) throws Exception {
        Optional<Profile> profileQuery = repository.getById(id);
        if(profileQuery.isPresent()) {
            repository.deleteProfileById(id);
        } else {
            throw new Exception("No existe pefil con ese id");
        }
    }

    public Optional<Profile> updateProfile(Profile profile, int id) throws Exception {
        Optional<Profile> profileData = repository.getById(id);
        if(profileData.isPresent()) {
            Profile newProfile = profileData.get();
            newProfile.setDocument(profile.getDocument());
            Optional<Profile> returnProf = repository.updateProfile(newProfile);
            return returnProf;
        }
        throw new Exception("No existe perfil con ese id");
    }


}
