package com.app.profiles.domain.service;

import com.app.profiles.domain.entity.Academics;
import com.app.profiles.domain.entity.Experience;
import com.app.profiles.domain.entity.User;
import com.app.profiles.persistence.repository.ExperienceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ExperienceService {

    @Autowired
    private ExperienceRepository repository;

    public List<Experience> getAll(String company) throws Exception {
        //List<Experience> experienceList = repository.getAll();
        List<Experience> experienceList = new ArrayList<Experience>();
        if (company == null) {
            repository.getAll().forEach(experienceList::add);
        } else {
            repository.getByCompany(company).forEach(experienceList::add);
        }

        //if(experienceList.isEmpty()) {
        //    throw new Exception("No hay datos de experiencia");
        //}
        return  experienceList;
    }

    public Experience getById(int id) throws Exception {
        Optional<Experience> experience = repository.getById(id);
        if(experience.isPresent()) {
            return experience.get();
        }
        throw new Exception("No hay datos de Experiencia con ese id");
    }

    public Experience saveExperience(Experience experience) throws Exception {
        experience = repository.saveExperience(experience);
        return experience;
    }

    public void deleteExperienceById(int id) throws Exception {
        Optional<Experience> experienceQuery = repository.getById(id);
        System.out.println("Llego al servicio");
        if(experienceQuery.isPresent()) {
            System.out.println("Encontro por id");
            repository.deleteExperienceById(id);
        } else {
            throw new Exception("No exisrwn datos de Experiencia con ese id");
        }
    }

    public Optional<Experience> updateExperience(Experience experience, int id) throws Exception {
        Optional<Experience> experienceData = repository.getById(id);
        if(experienceData.isPresent()) {
            Experience newExperience = experienceData.get();
            newExperience.setStartDate(experience.getStartDate());
            newExperience.setEndDate(experience.getEndDate());
            newExperience.setType(experience.getType());
            newExperience.setCompany(experience.getCompany());
            Optional<Experience> returnExp = repository.updateExperience(newExperience);
            //repository.updateExperience(newExperience);
            //return newExperience;
            return returnExp;
        }
        throw new Exception("No existe experiencia con este id");
    }



}
