package com.app.profiles.domain.gateway;

import com.app.profiles.domain.entity.User;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

public interface UserGateway {
    List<User> getAll() throws InvocationTargetException, IllegalAccessException;
    Optional<User> getById(int id);
    User saveUser(User user);
    void deleteUserById(int id);
}
