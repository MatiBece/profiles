package com.app.profiles.domain.gateway;

import com.app.profiles.domain.entity.Academics;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

public interface AcademicsGateway {
    List<Academics> getAll() throws InvocationTargetException, IllegalAccessException;
    Optional<Academics> getById(int id);
    Academics saveAcademic(Academics academics);
    void deleteAcademicsById(int id);
}
