package com.app.profiles.domain.gateway;

import com.app.profiles.domain.entity.Experience;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

public interface ExperienceGateway {
    List<Experience> getAll() throws InvocationTargetException, IllegalAccessException;
    Optional<Experience> getById(int id);
    Experience saveExperience(Experience experience);
    void deleteExperienceById(int id);
}
