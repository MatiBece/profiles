package com.app.profiles.domain.gateway;

import com.app.profiles.domain.entity.Profile;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

public interface ProfileGateway {
    List<Profile> getAll() throws InvocationTargetException, IllegalAccessException;
    Optional<Profile> getById(int id);
    Profile saveProfile(Profile profile);
    void deleteProfileById(int id);
}
