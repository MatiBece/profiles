package com.app.profiles.infrastructure.crud;

import com.app.profiles.domain.entity.Profile;
import com.app.profiles.persistence.model.ProfileDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProfileCrudRepository extends JpaRepository<ProfileDAO, Integer> {
    Optional<ProfileDAO> findByIdProfile(int idProfile);

    @Query(value = "select * from tbl_profiles inner join tbl_experience on tbl_profiles.id_profiles = tbl_experience.id_profile where (DATE_PART('year', tbl_experience.end_date) - DATE_PART('year', tbl_experience.start_date)) > :exp", nativeQuery = true)
    List<ProfileDAO> findByExperienceYears(Integer exp);

    @Query(value = "select * from tbl_profiles inner join tbl_academics on tbl_profiles.id_profiles = tbl_academics.id_profile where tbl_academics.type = :type or tbl_academics.type = :type2", nativeQuery = true)
    List<ProfileDAO> findByAcademicsType(String type, String type2);

    @Query(value = "select * from tbl_profiles inner join tbl_users on tbl_profiles.id_profiles = tbl_users.id_profile where tbl_users.country = :country or tbl_users.city = :city or tbl_users.transfer = :transfer", nativeQuery = true)
    List<ProfileDAO> findByUsersFilters(String country, String city, Boolean transfer);

    @Query(value = "select * from tbl_profiles inner join tbl_users on tbl_profiles.id_profiles = tbl_users.id_profile where tbl_users.tecnologies like %:technologies%", nativeQuery = true)
    List<ProfileDAO> findByUsersType(String technologies);

    @Query(value = "delete from tbl_profiles where id_profiles = :id ", nativeQuery = true)
    void deleteByIdProfile(int id);

    @Query(value = "delete from tbl_users where id_profiles = :id", nativeQuery = true)
    void deleteUserByIdProfile(int id);

    @Query(value = "update tbl_profiles set document = :document where id_profiles = :id", nativeQuery = true)
    Optional<ProfileDAO> updateProfileById(int id, String document);
}
