package com.app.profiles.infrastructure.crud;


import com.app.profiles.persistence.model.UserDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserCrudRepository extends JpaRepository<UserDAO, Integer> {
    Optional<UserDAO> findByIdUser(int idUser);

}
