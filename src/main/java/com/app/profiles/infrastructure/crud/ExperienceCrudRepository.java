package com.app.profiles.infrastructure.crud;

import com.app.profiles.persistence.model.ExperienceDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ExperienceCrudRepository extends JpaRepository<ExperienceDAO, Integer> {
    Optional<ExperienceDAO> findByIdExperience(int idExperience);

    @Query(value = "select * from tbl_experience where company = :company", nativeQuery = true)
    List<ExperienceDAO> findByCompanyContaining(String company);

    @Query(value = "delete from tbl_experience where id_experience = :id ", nativeQuery = true)
    void deleteByIdExperience(int id);

    /*@Query(value = "update tbl_experience set start_date = :startDate, end_date = :endDate, type = :type, company = :Company where id_experience = :idExperience", nativeQuery = true)
    default ExperienceDAO updateExperienceById(ExperienceDAO experienceDAO) {
        int idExperience = experienceDAO.getIdExperience();
        Date startDate = experienceDAO.getStartDate();
        Date endDate = experienceDAO.getEndDate();
        String type = experienceDAO.getType();
        String Company = experienceDAO.getCompany();
        return experienceDAO;
    };
     */

    @Query(value = "update tbl_experience set start_date = :startDate, end_date = :endDate, type = :type, company = :company where id_experience = :id", nativeQuery = true)
    Optional<ExperienceDAO> updateExperienceById(int id, Date startDate, Date endDate,  String type, String company);
}
