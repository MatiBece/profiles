package com.app.profiles.infrastructure.crud;

import com.app.profiles.persistence.model.AcademicsDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface AcademicsCrudRepository extends JpaRepository<AcademicsDAO, Integer> {
    Optional<AcademicsDAO> findByIdAcademics(int idAcademics);

    @Query(value = "select * from tbl_academics where type = :type or type = :type2", nativeQuery = true)
    List<AcademicsDAO> findByTypeContaining(String type, String type2);

    @Query(value = "delete from tbl_academics where id_academics = :id", nativeQuery = true)
    void deleteByIdAcademics(int id);

    @Query(value = "update tbl_academics set start_date = :startDate, end_date = :endDate, type = :type, university = :university, finished = :finished where id_academics = :id", nativeQuery = true)
    Optional<AcademicsDAO> updateAcademicsById(int id, Date startDate, Date endDate, String type, String university, Boolean finished);

}
