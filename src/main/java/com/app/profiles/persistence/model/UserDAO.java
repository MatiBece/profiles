package com.app.profiles.persistence.model;


import com.app.profiles.domain.entity.User;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "TBL_USERS")
public class UserDAO {

    @Id
    @Column(name = "id_user")
    private int idUser;
    private String name;
    private String lastName;
    private String email;
    private int experience;
    private int age;
    private String tecnologies;
    private boolean transfer;
    private Calendar birthDate;
    private String country;
    private String city;
    @Column(name = "id_profile")
    private int idProfile;

    @OneToOne
    @JoinColumn(name = "id_profile", insertable = false, updatable = false)
    private ProfileDAO profileDAO;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTecnologies() {
        return tecnologies;
    }

    public void setTecnologies(String tecnologies) {
        this.tecnologies = tecnologies;
    }

    public boolean isTransfer() {
        return transfer;
    }

    public void setTransfer(boolean transfer) {
        this.transfer = transfer;
    }

    public Calendar getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Calendar birthDate) {
        this.birthDate = birthDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(int idProfile) {
        this.idProfile = idProfile;
    }

    public static User userDAOToUser(UserDAO userDAO){
        User user = new User();
        user.setIdUser(userDAO.getIdUser());
        user.setName(userDAO.getName());
        user.setLastName(userDAO.getLastName());
        user.setEmail(userDAO.getEmail());
        user.setExperience(userDAO.getExperience());
        user.setAge(userDAO.getAge());
        user.setTecnologies(userDAO.getTecnologies());
        user.setTransfer(userDAO.isTransfer());
        user.setBirthDate(userDAO.getBirthDate());
        user.setCountry(userDAO.getCountry());
        user.setCity(userDAO.getCity());
        user.setIdProfile(userDAO.getIdUser());
        return user;
    }

    public static UserDAO userToUserDAO(User user){
        UserDAO userDAO = new UserDAO();
        userDAO.setIdUser(user.getIdUser());
        userDAO.setName(user.getName());
        userDAO.setLastName(user.getLastName());
        userDAO.setEmail(user.getEmail());
        userDAO.setExperience(user.getExperience());
        userDAO.setAge(user.getAge());
        userDAO.setTecnologies(user.getTecnologies());
        userDAO.setTransfer(user.isTransfer());
        userDAO.setBirthDate(user.getBirthDate());
        userDAO.setCountry(user.getCountry());
        userDAO.setCity(user.getCity());
        userDAO.setIdProfile(user.getIdProfile());
        return userDAO;
    }
}
