package com.app.profiles.persistence.model;

import com.app.profiles.domain.entity.Academics;
import com.app.profiles.domain.entity.Experience;
import com.app.profiles.domain.entity.Profile;
import com.app.profiles.domain.entity.User;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "TBL_PROFILES")
public class ProfileDAO {

    @Id
    @Column(name = "id_profiles")
    private int idProfile;
    private String document;

    @OneToOne(mappedBy = "profileDAO")
    private UserDAO userDAO;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany( mappedBy = "profileDAO", cascade = CascadeType.ALL)
    private List<AcademicsDAO> academicsDAOList;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany( mappedBy = "profileDAO", cascade = CascadeType.ALL)
    private List<ExperienceDAO> experienceDAOList;

    public int getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(int idProfile) {
        this.idProfile = idProfile;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public List<AcademicsDAO> getAcademicsDAOList() {
        return academicsDAOList;
    }

    public void setAcademicsDAOList(List<AcademicsDAO> academicsDAOList) {
        this.academicsDAOList = academicsDAOList;
    }

    public List<ExperienceDAO> getExperienceDAOList() {
        return experienceDAOList;
    }

    public void setExperienceDAOList(List<ExperienceDAO> experienceDAOList) {
        this.experienceDAOList = experienceDAOList;
    }

    public static Profile profileDAOToProfile(ProfileDAO profileDAO){
        Profile profile = new Profile();
        profile.setIdProfile(profileDAO.getIdProfile());
        profile.setDocument(profileDAO.getDocument());
        profile.setExperience(ExperienceDAO.experienceDAOListToExperienceList(profileDAO.getExperienceDAOList()));
        profile.setAcademics(AcademicsDAO.academicsDAOListToAcademicsList(profileDAO.getAcademicsDAOList()));
        profile.setUser(UserDAO.userDAOToUser(profileDAO.getUserDAO()));
        return profile;
    }

    public static ProfileDAO profileToProfileDAO(Profile profile){
        ProfileDAO profileDAO = new ProfileDAO();
        profileDAO.setIdProfile(profile.getIdProfile());
        profileDAO.setDocument(profile.getDocument());
        profileDAO.setExperienceDAOList(ExperienceDAO.experienceListToExperienceDAOList(profile.getExperience()));
        profileDAO.setAcademicsDAOList(AcademicsDAO.academicsListToAcademicsDAOList(profile.getAcademics()));
        profileDAO.setUserDAO(UserDAO.userToUserDAO(profile.getUser()));
        return profileDAO;
    }

    public static Profile profileDAOToProfileSave(ProfileDAO profileDAO) {
        Profile profile = new Profile();
        profile.setIdProfile(profileDAO.getIdProfile());
        profile.setDocument(profileDAO.getDocument());
        return profile;
    }

    public static ProfileDAO profileToProfileDAOSave(Profile profile) {
        ProfileDAO profileDAO = new ProfileDAO();
        profileDAO.setIdProfile(profile.getIdProfile());
        profileDAO.setDocument(profile.getDocument());
        return profileDAO;
    }
}
