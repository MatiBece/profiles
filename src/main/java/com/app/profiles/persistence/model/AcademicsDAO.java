package com.app.profiles.persistence.model;

import com.app.profiles.domain.entity.Academics;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "TBL_ACADEMICS")
public class AcademicsDAO {
    @Id
    @Column(name = "id_academics")
    private int idAcademics;
    private Date startDate;
    private Date endDate;
    private String type;
    private String university;
    private boolean finished;
    @Column(name = "id_profile")
    private int idProfile;

    @ManyToOne
    @JoinColumn(name = "id_profile", insertable = false, updatable = false)
    private  ProfileDAO profileDAO;

    public int getIdAcademics() {
        return idAcademics;
    }

    public void setIdAcademics(int idAcademics) {
        this.idAcademics = idAcademics;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public int getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(int idProfile) {
        this.idProfile = idProfile;
    }

    public ProfileDAO getProfileDAO() {
        return profileDAO;
    }

    public void setProfileDAO(ProfileDAO profileDAO) {
        this.profileDAO = profileDAO;
    }

    public static Academics academicsDAOToAcademics(AcademicsDAO academicsDAO) {
        Academics academics = new Academics();
        academics.setIdAcademics(academicsDAO.getIdAcademics());
        academics.setStartDate(academicsDAO.getStartDate());
        academics.setEndDate(academicsDAO.getEndDate());
        academics.setType(academicsDAO.getType());
        academics.setUniversity(academicsDAO.getUniversity());
        academics.setIdProfile(academicsDAO.getIdProfile());
        academics.setFinished(academicsDAO.isFinished());
        //academics.setProfile(ProfileDAO.profileDAOToProfile(academicsDAO.profileDAO));
        return academics;
    }

    public static List<Academics> academicsDAOListToAcademicsList(List<AcademicsDAO> academicsDAOList) {
        List<Academics> newListAcademics = new ArrayList<>();
        academicsDAOList.forEach(x -> newListAcademics.add(AcademicsDAO.academicsDAOToAcademics(x)));
        return newListAcademics;
    }

    public static List<AcademicsDAO> academicsListToAcademicsDAOList(List<Academics> academicsList) {
        List<AcademicsDAO> newListAcademicsDAO = new ArrayList<>();
        academicsList.forEach(x -> newListAcademicsDAO.add(AcademicsDAO.academicsToAcademicsDAO(x)));
        return newListAcademicsDAO;
    }

    public static AcademicsDAO academicsToAcademicsDAO(Academics academics){
        AcademicsDAO academicsDAO = new AcademicsDAO();
        academicsDAO.setIdAcademics(academics.getIdAcademics());
        academicsDAO.setStartDate(academics.getStartDate());
        academicsDAO.setEndDate(academics.getEndDate());
        academicsDAO.setType(academics.getType());
        academicsDAO.setUniversity(academics.getUniversity());
        academicsDAO.setIdProfile(academics.getIdProfile());
        academicsDAO.setFinished(academics.isFinished());
        /////////////////////
        //academicsDAO.setProfileDAO(ProfileDAO.profileToProfileDAO(academics.getProfile()));
        return academicsDAO;
    }
}
