package com.app.profiles.persistence.model;

import com.app.profiles.domain.entity.Experience;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "TBL_EXPERIENCE")
public class ExperienceDAO {

    @Id
    @Column( name = "id_experience")
    private int idExperience;
    private Date startDate;
    private Date endDate;
    private String type;
    private String company;
    @Column(name = "id_profile")
    private int idProfile;

    @ManyToOne
    @JoinColumn(name = "id_profile", insertable = false, updatable = false)
    private  ProfileDAO profileDAO;

    public int getIdExperience() {
        return idExperience;
    }

    public void setIdExperience(int idExperience) {
        this.idExperience = idExperience;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(int idProfile) {
        this.idProfile = idProfile;
    }

    @Override
    public String toString() {
        return "ExperienceDAO{" +
                "idExperience=" + idExperience +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", type='" + type + '\'' +
                ", Company='" + company + '\'' +
                ", idProfile=" + idProfile +
                ", profileDAO=" + profileDAO +
                '}';
    }

    public static Experience experienceDAOToExperience(ExperienceDAO experienceDAO) {
        Experience experience = new Experience();
        experience.setIdExperience(experienceDAO.getIdExperience());
        experience.setStartDate(experienceDAO.getStartDate());
        experience.setEndDate(experienceDAO.getEndDate());
        experience.setType(experienceDAO.getType());
        experience.setCompany(experienceDAO.getCompany());
        experience.setIdProfile(experienceDAO.getIdProfile());
        return experience;
    }

    public static List<Experience> experienceDAOListToExperienceList(List<ExperienceDAO> experienceDAOList) {
        List<Experience> newListExperience = new ArrayList<>();
        experienceDAOList.forEach(x -> newListExperience.add(ExperienceDAO.experienceDAOToExperience(x)));
        return newListExperience;
    }

    public static List<ExperienceDAO> experienceListToExperienceDAOList(List<Experience> experienceList) {
        List<ExperienceDAO> newListExperienceDAO = new ArrayList<>();
        experienceList.forEach(x -> newListExperienceDAO.add(ExperienceDAO.experienceToExperienceDAO(x)));
        return newListExperienceDAO;
    }

    public static ExperienceDAO experienceToExperienceDAO(Experience experience) {
        ExperienceDAO experienceDAO = new ExperienceDAO();
        experienceDAO.setIdExperience(experience.getIdExperience());
        experienceDAO.setStartDate(experience.getStartDate());
        experienceDAO.setEndDate(experience.getEndDate());
        experienceDAO.setType(experience.getType());
        experienceDAO.setCompany(experience.getCompany());
        experienceDAO.setIdProfile(experience.getIdProfile());
        return experienceDAO;
    }
}
