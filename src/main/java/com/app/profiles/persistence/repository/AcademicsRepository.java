package com.app.profiles.persistence.repository;

import com.app.profiles.domain.entity.Academics;
import com.app.profiles.domain.gateway.AcademicsGateway;
import com.app.profiles.infrastructure.crud.AcademicsCrudRepository;
import com.app.profiles.persistence.model.AcademicsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public class AcademicsRepository implements AcademicsGateway {

    @Autowired
    private AcademicsCrudRepository crudRepository;

    @Override
    public List<Academics> getAll() {
        List<AcademicsDAO> academicsDAOList = (List<AcademicsDAO>) crudRepository.findAll();
        List<Academics> academicsList = new ArrayList<>();
        academicsDAOList.forEach(x -> academicsList.add(AcademicsDAO.academicsDAOToAcademics(x)));
        return academicsList;
    }

    @Override
    public Optional<Academics> getById(int id) {
        Optional<AcademicsDAO> queriedAcademics = crudRepository.findById(id);
        if (queriedAcademics.get() == null) {
            throw new RuntimeException("No existen datos academicos");
        }
        return Optional.of(AcademicsDAO.academicsDAOToAcademics(queriedAcademics.get()));
    }

    @Override
    public Academics saveAcademic(Academics academics) {
        AcademicsDAO academics1 = crudRepository.save(AcademicsDAO.academicsToAcademicsDAO(academics));
        return AcademicsDAO.academicsDAOToAcademics(academics1);
    }

    @Override
    public void deleteAcademicsById(int id) {
        Optional<AcademicsDAO> academicsDAO = crudRepository.findById(id);
        if (academicsDAO.isPresent()) {
            crudRepository.deleteByIdAcademics(id);
        }
    }

    public List<Academics> getByType(String type, String type2) {
        List<AcademicsDAO> queriedAcademics = crudRepository.findByTypeContaining(type, type2);
        if (queriedAcademics == null) {
            throw new RuntimeException("No existen datos academicos");
        }
        List<Academics> academics = new ArrayList<Academics>();

        queriedAcademics.forEach( x -> academics.add(AcademicsDAO.academicsDAOToAcademics(x)));
        return academics;
    }

    public Optional<Academics> updateAcademics(Academics academics) {
        AcademicsDAO newAcademics = AcademicsDAO.academicsToAcademicsDAO(academics);
        int id = newAcademics.getIdAcademics();
        Date startDate = newAcademics.getStartDate();
        Date endDate = newAcademics.getEndDate();
        String type = newAcademics.getType();
        String university = newAcademics.getUniversity();
        Boolean finished = newAcademics.isFinished();
        crudRepository.updateAcademicsById(id, startDate, endDate, type, university, finished);
        return Optional.of(AcademicsDAO.academicsDAOToAcademics(newAcademics));
    }
}
