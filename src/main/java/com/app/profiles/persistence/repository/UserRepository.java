package com.app.profiles.persistence.repository;

import com.app.profiles.domain.entity.User;
import com.app.profiles.domain.gateway.UserGateway;

import com.app.profiles.infrastructure.crud.UserCrudRepository;
import com.app.profiles.persistence.model.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Repository
public class UserRepository implements UserGateway {

    @Autowired
    private UserCrudRepository crudRepository;

    @Override
    public List<User> getAll(){
        List<UserDAO> userDAOList = (List<UserDAO>) crudRepository.findAll();
        List<User> userList = new ArrayList<>();
        userDAOList.forEach(x -> userList.add(UserDAO.userDAOToUser(x)));
        return userList;
    }

    @Override
    public Optional<User> getById(int id) {
        Optional<UserDAO> queriedUser = crudRepository.findById(id);
        if(queriedUser.get() == null){
            throw new RuntimeException("No se encuentra");
        }
        return Optional.of(UserDAO.userDAOToUser(queriedUser.get()));
    }

    @Override
    public User saveUser(User user) {
        UserDAO user1 = crudRepository.save(UserDAO.userToUserDAO(user));
        return UserDAO.userDAOToUser(user1);
    }

    @Override
    public void deleteUserById(int id) {
        Optional<UserDAO> userDAO =crudRepository.findById(id);
        System.out.println("llego hasta repository");
        if(userDAO.isPresent()){
            crudRepository.deleteById(id);
        }
    }

}
