package com.app.profiles.persistence.repository;

import com.app.profiles.domain.entity.Experience;
import com.app.profiles.domain.entity.Profile;
import com.app.profiles.domain.gateway.ProfileGateway;
import com.app.profiles.infrastructure.crud.ProfileCrudRepository;
import com.app.profiles.infrastructure.crud.UserCrudRepository;
import com.app.profiles.persistence.model.ProfileDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ProfileRepository implements ProfileGateway {

    @Autowired
    private ProfileCrudRepository crudRepository;

    @Autowired
    private UserCrudRepository userCrudRepository;

    @Override
    public List<Profile> getAll() {
        List<ProfileDAO> profileDAOList = (List<ProfileDAO>) crudRepository.findAll();
        List<Profile> profileList = new ArrayList<>();
        profileDAOList.forEach(x -> profileList.add(ProfileDAO.profileDAOToProfile(x)));
        return profileList;
    }

    @Override
    public Optional<Profile> getById(int id) {
        Optional<ProfileDAO> queriedProfile = crudRepository.findById(id);
        if (queriedProfile.get() == null) {
            throw new RuntimeException("No se encuentra");
        }
        return Optional.of(ProfileDAO.profileDAOToProfile(queriedProfile.get()));
    }

    @Override
    public Profile saveProfile(Profile profile) {
        ProfileDAO newProfile = crudRepository.save(ProfileDAO.profileToProfileDAOSave(profile));
        return ProfileDAO.profileDAOToProfileSave(newProfile);
    }

    @Override
    public void deleteProfileById(int id) {
        Optional<ProfileDAO> profileDAO = crudRepository.findById(id);
        if (profileDAO.isPresent()) {
            //userCrudRepository.deleteById(id);
            userCrudRepository.deleteById(id);
            crudRepository.deleteByIdProfile(id);
            return;
        }
    }

    public List<Profile> getByExperienceYear(Integer exp) {
        List<ProfileDAO> queriedProfiles = crudRepository.findByExperienceYears(exp);
        if (queriedProfiles == null) {
            throw new RuntimeException("No existen datos de perfil");
        }
        List<Profile> profiles = new ArrayList<Profile>();
        queriedProfiles.forEach(x -> profiles.add(ProfileDAO.profileDAOToProfile(x)));
        List<Experience> experiences = new ArrayList<Experience>();
        return profiles;
    }

    public List<Profile> getByAcademicsType(String type, String type2) {
        List<ProfileDAO> queriedProfiles = crudRepository.findByAcademicsType(type, type2);
        if (queriedProfiles == null) {
            throw new RuntimeException("No existen datos de perfil");
        }
        List<Profile> profiles = new ArrayList<Profile>();

        queriedProfiles.forEach(x -> profiles.add(ProfileDAO.profileDAOToProfile(x)));
        return profiles;
    }

    public List<Profile> getByUsersFilters(String country, String city, Boolean transfer) {
        List<ProfileDAO> queriedProfiles = crudRepository.findByUsersFilters(country, city, transfer);
        queriedProfiles.forEach(x -> System.out.println("datos " + x.getIdProfile()));
        if (queriedProfiles == null) {
            throw new RuntimeException("No existen datos de perfil");
        }
        List<Profile> profiles = new ArrayList<Profile>();
        queriedProfiles.forEach(x -> profiles.add(ProfileDAO.profileDAOToProfile(x)));
        return profiles;
    }

    public List<Profile> getByUsersType(String technologies) {
        List<ProfileDAO> queriedProfiles = crudRepository.findByUsersType(technologies);
        if (queriedProfiles == null) {
            throw new RuntimeException("No existen datos de perfil");
        }
        List<Profile> profiles = new ArrayList<Profile>();
        queriedProfiles.forEach(x -> profiles.add(ProfileDAO.profileDAOToProfile(x)));
        return profiles;
    }

    public Optional<Profile> updateProfile(Profile profile) {
        ProfileDAO newProfile = ProfileDAO.profileToProfileDAO(profile);

        int id = newProfile.getIdProfile();
        String document = newProfile.getDocument();
        crudRepository.updateProfileById(id, document);
        return Optional.of(ProfileDAO.profileDAOToProfile(newProfile));
    }
}
