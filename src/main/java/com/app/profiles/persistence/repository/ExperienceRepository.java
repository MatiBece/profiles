package com.app.profiles.persistence.repository;

import com.app.profiles.domain.entity.Experience;
import com.app.profiles.domain.gateway.ExperienceGateway;
import com.app.profiles.infrastructure.crud.AcademicsCrudRepository;
import com.app.profiles.infrastructure.crud.ExperienceCrudRepository;
import com.app.profiles.persistence.model.AcademicsDAO;
import com.app.profiles.persistence.model.ExperienceDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ExperienceRepository implements ExperienceGateway {

    @Autowired
    private ExperienceCrudRepository crudRepository;

    @Override
    public List<Experience> getAll() {
        List<ExperienceDAO> experienceDAOList = (List<ExperienceDAO>) crudRepository.findAll();


        List<Experience> experienceList = new ArrayList<>();
        experienceDAOList.forEach(x -> experienceList.add(ExperienceDAO.experienceDAOToExperience(x)));
        return experienceList;
    }

    @Override
    public Optional<Experience> getById(int id) {
        Optional<ExperienceDAO> queriedExperience = crudRepository.findById(id);
        if(queriedExperience.get() == null) {
            throw new RuntimeException("No existen datos de experiencia");
        }
        return Optional.of(ExperienceDAO.experienceDAOToExperience(queriedExperience.get()));
    }


    @Override
    public Experience saveExperience(Experience experience) {
        ExperienceDAO newExperience = crudRepository.save(ExperienceDAO.experienceToExperienceDAO(experience));
        return ExperienceDAO.experienceDAOToExperience(newExperience);
    }

    @Override
    public void deleteExperienceById(int id) {
        Optional<ExperienceDAO> experienceDAO = crudRepository.findById(id);
        if(experienceDAO.isPresent()) {
            crudRepository.deleteByIdExperience(id);
        }
    }


    public List<Experience> getByCompany(String company) {
        List<ExperienceDAO> queriedExperience = crudRepository.findByCompanyContaining(company);
        if(queriedExperience == null) {
            throw new RuntimeException("No existen datos de experiencia");
        }
        List<Experience> experiences = new ArrayList<Experience>();

        queriedExperience.forEach(x -> experiences.add(ExperienceDAO.experienceDAOToExperience(x)));
        //return Optional.of(ExperienceDAO.experienceDAOToExperience(queriedExperience.get()));
        return experiences;
    }


    public Optional<Experience> updateExperience(Experience experience) {

        ExperienceDAO newExperience = ExperienceDAO.experienceToExperienceDAO(experience);

        int id = newExperience.getIdExperience();
        Date startDate = newExperience.getStartDate();
        Date endDate = newExperience.getEndDate();
        String type = newExperience.getType();
        String company = newExperience.getCompany();
        crudRepository.updateExperienceById(id, startDate, endDate, type, company);
        return Optional.of(ExperienceDAO.experienceDAOToExperience(newExperience));
    }


}
