package com.app.profiles.persistence.controller;

import com.app.profiles.domain.entity.Academics;
import com.app.profiles.domain.entity.Experience;
import com.app.profiles.domain.service.AcademicsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AcademicsController {

    @Autowired
    private AcademicsService service;

    //GET ALL ACADEMICS METHOD
    //@GetMapping("/academics")
    //public ResponseEntity<List<Academics>> getAll() throws Exception{
    //    return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    //}

    @GetMapping("/academics")
    public ResponseEntity<List<Academics>> getAll(@RequestParam(required = false) String type, @RequestParam(required = false) String type2) throws Exception {
        return new ResponseEntity<>(service.getAll(type, type2), HttpStatus.OK);
    }


    // GET 1 ACADEMICS BY ID METHOD
    @GetMapping("/academics/{id}")
    public ResponseEntity<Academics> findById(@PathVariable("id") int id) throws Exception {
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }

    // POST ACADEMICS METHOD
    @PostMapping("/academics")
    public ResponseEntity<Academics> saveAcademics(@RequestBody Academics academics) throws Exception {
        return new ResponseEntity<>(service.saveAcademics(academics), HttpStatus.CREATED);
    }

    // PUT ACADEMICS METHOD
    @PutMapping("/academics/{id}")
    public ResponseEntity<Optional<Academics>> updateAcademics(@RequestBody Academics academics, @PathVariable("id") int id) throws Exception {
        return new ResponseEntity<>(service.updateAcademics(academics, id), HttpStatus.CREATED);
    }

    // DELETE ACADEMICS METHOD
    @DeleteMapping("/academics/{id}")
    public ResponseEntity deleteAcademics(@PathVariable("id") int id) throws Exception {
        service.deleteAcademicsById(id);
        return new ResponseEntity(HttpStatus.OK);
    }


}
