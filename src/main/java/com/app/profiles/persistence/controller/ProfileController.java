package com.app.profiles.persistence.controller;


import com.app.profiles.domain.entity.Profile;
import com.app.profiles.domain.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProfileController {

    @Autowired
    private ProfileService service;

    // GET ALL PROFILES METHOD WITH FILTERS
    @GetMapping("/profiles")
    public  ResponseEntity<List<Profile>> getAll(@RequestParam(required = false) String type, @RequestParam(required = false) String type2, @RequestParam(required = false) String country, @RequestParam(required = false) String city, @RequestParam(required = false) Boolean transfer, @RequestParam(required = false) String technologies, @RequestParam(required = false) Integer exp) throws Exception {
        return new ResponseEntity<>(service.getAll(type, type2, country, city, transfer, technologies, exp), HttpStatus.OK);
    }


    @GetMapping("/profiles/{id}")
    public ResponseEntity<Profile> findById(@PathVariable("id") int id) throws Exception {
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }

    @PostMapping("/profiles")
    public ResponseEntity<Profile> saveProfile(@RequestBody Profile profile) throws Exception {
        return new ResponseEntity<>(service.saveProfile(profile), HttpStatus.CREATED);
    }

    @DeleteMapping("/profiles/{id}")
    public ResponseEntity deleteProfile(@PathVariable("id") int id) throws Exception {
        service.deleteProfileById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/profiles/{id}")
    public ResponseEntity<Optional<Profile>> updateProfile(@RequestBody Profile profile, @PathVariable("id") int id) throws Exception {
        return new ResponseEntity<>(service.updateProfile(profile, id), HttpStatus.CREATED);
    }



}

/*
 ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠰⣶⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇⢻⡆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⣠⣤⣤⠶⠶⠶⠦⣤⣄⣠⠇⠈⡿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣤⠴⠚⠋⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠀⠸⠛⢻⣷⣦⣀⣤⣤⣤⣤⣶⣶⣶⣦⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣴⠞⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⣿⣿⡀⠛⢿⣿⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣤⡾⠋⢀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠱⡀⠀⠀⠀⠐⢤⡀⠸⣿⣷⣄⠾⢿⣿⣿⣿⣿⣿⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣴⡾⠋⠀⢠⠎⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡀⠀⠀⠀⠳⡄⠀⠀⠀⠀⠱⣤⠙⢿⣿⣦⡸⠿⢿⣿⣿⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⡾⠋⠀⠀⢰⠏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢧⠀⠀⠀⠀⢱⡀⠀⠀⠀⠀⢸⢆⠀⠻⣿⣿⣶⣜⡛⢻⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣄⠀⠀⠀⠀⠀⠀⢀⣠⣴⠟⠀⠀⠀⢠⠃⠀⠀⠀⠀⠀⠀⠀⠀⣆⠀⠀⠀⠸⣶⡀⠀⠀⠀⢳⠀⠀⠀⠀⠈⡌⢧⠀⠈⠛⢿⣿⣿⣿⣿⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡟⠀⠀⠀⢀⣤⣾⣿⣿⠋⠀⠀⠀⠀⣼⠀⠀⠀⠀⠀⠀⣄⠀⠀⣾⣄⠀⠀⠀⢹⣧⡀⠀⠀⠈⣇⠀⠀⠀⠀⡇⢸⣆⠀⠀⠘⣿⠛⠻⠟⢻⡆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠐⠲⠶⣯⢻⣤⣤⡀⠘⣿⣿⣿⠃⠀⠀⠀⠀⠀⣇⠀⠀⠀⠀⠀⠀⣿⠀⠀⣿⣸⠦⣄⠀⠀⢻⣷⣄⠀⠀⠸⡄⠀⠀⠀⡇⡞⠘⡆⠀⠀⢯⣦⠀⠀⠀⢿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠸⡼⠀⠀⠀⠀⢻⣿⠏⠀⠀⠀⣄⠀⢰⣿⠀⠀⠀⠀⠀⠀⣿⢷⡀⠸⣶⣿⣿⣿⣷⡲⢿⣿⣷⣄⡀⢻⡀⠀⠀⠃⡇⠀⢱⡀⠀⢸⣿⡆⠀⠀⠈⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠇⠀⠀⠀⠀⠈⣿⠀⢰⠆⢠⣷⠀⢸⠈⢷⣄⠀⠀⠀⠀⢻⠀⠩⣿⠿⠇⢹⣿⣿⣿⡄⠙⢿⣿⣿⡿⡇⠀⢸⢸⠃⠀⠈⣇⠀⠸⢻⢇⠀⠀⠀⣿⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢰⡇⠀⢸⡄⣼⣿⣷⣼⣷⣦⠈⠙⠓⠒⠒⠚⠛⠀⠟⢀⣷⣾⣿⣿⠙⣿⠀⠈⢿⣧⣤⣿⠀⣼⡜⠀⠀⠀⢹⠀⠀⣼⢸⠀⠀⠀⡏⢧⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⡇⠀⠈⣇⣿⣿⢟⣿⣿⣷⡄⠀⠀⠀⠀⠀⠀⠀⠀⠸⡟⡿⠋⢻⠀⡿⠀⠀⠈⣿⣤⣿⣤⣧⠃⠀⠀⠀⠈⡄⠀⣿⢸⡄⠀⠀⣧⠈⢷⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡆⠀⢸⡇⢰⣦⣬⣾⢧⡎⣽⣿⣿⡧⠀⠀⠀⠀⠀⠀⠀⠀⠀⢷⡙⠒⠋⡼⠁⠀⠀⠀⣿⠶⢼⣿⡿⠀⠀⠀⠀⠀⣇⠀⣿⠈⡇⠀⠀⣿⡄⠈⢷⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢤⣼⣇⣀⠈⢷⡀⣸⣿⡇⣸⣿⡿⢿⡟⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⠀⠉⠀⠀⠀⠀⠰⠟⠆⢸⣿⠃⠀⠀⠀⠀⠀⢹⠀⣿⠀⡇⠀⠀⣿⡇⠀⠀⠳⡄⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⡏⠀⠀⣾⠡⣿⣿⡃⢻⢿⣀⣸⢁⠇⠀⠀⡆⠀⠀⠀⠀⠀⠀⠀⢠⣤⠄⠄⠀⠀⠀⠀⠀⠀⣼⡇⠀⠀⠀⠀⠀⠀⢸⢸⡟⠀⡇⠀⢰⢹⣶⠀⠀⠀⣿⣆⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⠁⠀⣸⡇⡄⣿⣿⡀⠀⢷⣉⡁⠞⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⢀⠀⡄⠀⡆⠀⠀⠀⢀⡟⠀⠀⠀⠀⠀⠀⠀⢸⣟⠇⠀⡇⠀⣸⠀⣿⠀⠀⠀⠘⣿⣆⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡟⡇⣧⢻⣿⣇⠀⠀⠈⠀⠀⠀⣠⠖⠋⠉⠙⠓⣦⣄⠀⠘⢀⡇⢸⠀⠀⠃⠀⠀⢠⡟⠀⠀⠀⠀⠀⠀⠀⠀⢸⡿⠀⠀⡇⢀⡏⢀⣿⠀⠀⠀⠀⢻⣿⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣾⠀⡇⢸⢸⠉⠀⠊⠶⠘⠀⠀⢸⣥⣤⣤⠶⠒⠉⠁⠈⠳⣄⠈⠀⠀⠀⠀⠀⠀⠀⡞⠁⠀⠀⠀⠀⠀⠀⠀⠀⢸⡇⠀⢠⡇⣸⠁⢸⢿⠀⠀⠀⠀⢸⢻⡅⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⠃⠀⢳⠈⣿⠀⠀⡄⡴⢠⡆⠀⣿⣿⡿⠃⠀⠀⠀⠀⠀⠀⠈⢳⡀⠀⠀⠀⠀⠀⣼⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⠃⠀⢸⢡⠃⠀⣸⢸⠀⠀⠀⠀⡼⢸⡇⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⡏⠀⠀⠈⡆⢹⡇⢸⠁⠃⠀⠁⠀⢹⣿⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇⠀⠀⠀⠀⣸⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣾⠀⢀⣯⠏⠀⢀⠇⡸⠀⠀⠀⢠⡇⢸⡇⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣾⠀⢀⠀⠀⢹⡈⣿⡀⠀⠀⠀⠀⠀⠘⣏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⠀⠀⠀⢠⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣿⣿⣿⣿⣿⣷⣾⣤⣇⠀⠀⢀⡞⠀⣼⠇⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⡇⢀⣿⠀⠀⠀⢣⡈⠻⣦⣤⣀⠀⠀⠀⠘⢦⡀⠀⠀⠀⠀⠀⠀⠀⣠⠇⠀⠀⢀⣾⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣏⡀⢰⡟⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣼⠀⣸⣿⠀⠀⠀⠀⠳⠀⠹⣹⡿⢿⣶⣦⣤⣄⣉⣓⣲⣤⣤⣴⣒⣋⣡⣤⣶⢾⣫⣿⡃⠀⠀⠀⠀⠀⠀⠀⠀⠀⣸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣄⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⢀⣿⣿⠀⠀⠀⠀⠀⠀⠳⣵⣵⡈⢹⡿⣿⣿⣿⣿⣿⠩⣟⣿⣿⣿⣿⢽⠖⡍⢠⢻⡇⠀⠀⠀⠀⠀⠀⠀⠀⣰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⢸⡟⠸⡄⠀⠀⠀⠀⠀⠀⠈⠻⣷⠀⣇⣿⣿⣿⣿⣿⣏⡆⢰⠸⠀⠇⢘⠀⣁⢠⢨⢧⠀⠀⠀⠀⠀⠀⢀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠃⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢹⣿⡇⠀⢻⡀⠀⠀⠀⠀⠀⠀⠀⠘⢷⣸⣿⣿⣿⣿⣿⣿⣿⣦⣢⡐⣌⢎⢧⠘⣎⢯⣿⣷⣀⠀⠀⠀⣠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠇⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢿⣇⠀⠀⠻⣤⡀⠀⠀⠀⠀⠀⠀⠈⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣜⣞⣆⢣⢸⣾⣿⣿⣿⣇⣤⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠙⠀⠀⠀⠈⠛⢦⣄⣀⠀⠀⠀⠀⠀⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾⣾⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡄⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠉⠙⠛⠒⠚⣶⠾⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⠋⠉⠉⠉⠛⠻⢯⡄⠹⠿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣆⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣀⣴⠋⠁⠀⣀⣙⡿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣇⡴⠶⠖⠒⠀⠀⠈⢻⠀⠀⠘⢛⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡄⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⡾⢉⡽⠁⣠⡴⠛⠉⠁⢹⣾⣿⣿⣿⣿⣿⣿⣿⣿⣹⡄⠀⣀⣠⠤⠀⠀⠀⠈⣟⠀⠀⠀⠉⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢰⡃⠀⣿⠇⠀⢀⡀⡤⠔⠒⠺⣯⣿⣿⣿⠿⣿⣿⠛⣻⠧⡟⠉⠁⠀⠀⠀⠀⠀⠀⡽⠃⠀⠀⠀⠈⣻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣼⠁⠀⢿⡀⠀⠀⠀⠀⠀⣀⣀⣿⣿⣿⣿⣯⣽⣿⣄⣨⣯⣧⡤⠴⠒⠊⠁⠀⠀⠀⢹⠆⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣻⠀⠀⢼⡇⠀⠀⠀⠶⠋⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⠀⠀⢺⡀⠀⠀⠀⠀⢀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢻⡄⠀⠀⢹⡀⠀⠀⠀⣠⣴⠿⣻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣽⣯⣝⣀⢰⡋⠀⠀⠀⠀⠀⢀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣇⠀⠀⠈⢷⣄⣀⣠⣿⡏⠘⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⠘⠻⠟⠁⠀⠀⠀⠀⠀⣨⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
 */