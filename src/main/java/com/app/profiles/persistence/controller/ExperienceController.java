package com.app.profiles.persistence.controller;

import com.app.profiles.domain.entity.Experience;
import com.app.profiles.domain.service.ExperienceService;
import com.app.profiles.infrastructure.crud.ExperienceCrudRepository;
import com.app.profiles.persistence.model.ExperienceDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class ExperienceController {

    @Autowired
    private ExperienceService service;

    @Autowired
    private ExperienceCrudRepository repository;

    //@GetMapping("/experiences")
    //public ResponseEntity<List<Experience>> getAll() throws Exception {
    //    return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    //}

    @GetMapping("/experiences")
    public ResponseEntity<List<Experience>> getAll(@RequestParam(required = false) String company) throws Exception {
        return new ResponseEntity<>(service.getAll(company), HttpStatus.OK);
    }

    /*@GetMapping("/experiences")
    public ResponseEntity<List<Experience>> getAll(@RequestParam(required = false) String company) throws Exception {
        List<Experience> experienceList = new ArrayList<Experience>();
        List<ExperienceDAO> experienceDAOList = repository.findAll();

        if (company == null) {
            experienceList = ExperienceDAO.experienceDAOListToExperienceList(experienceDAOList);
        } else {
            List<ExperienceDAO> experienceDAOListCompany = repository.findByCompanyContaining(company);
            experienceList = ExperienceDAO.experienceDAOListToExperienceList(experienceDAOListCompany);
        }

        return new ResponseEntity<>(experienceList, HttpStatus.OK);
    }*/

    @GetMapping("/experiences/{id}")
    public ResponseEntity<Experience> findById(@PathVariable("id") int id) throws Exception {
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }

    @PostMapping("/experiences")
    public ResponseEntity<Experience> saveExperience(@RequestBody Experience experience) throws Exception {
        return new ResponseEntity<>(service.saveExperience(experience), HttpStatus.CREATED);
    }

    @PutMapping("/experiences/{id}")
    public ResponseEntity<Optional<Experience>> updateExperience(@RequestBody Experience experience, @PathVariable("id") int id) throws Exception {
        return new ResponseEntity<>(service.updateExperience(experience, id), HttpStatus.CREATED);
    }

    @DeleteMapping("/experiences/{id}")
    public ResponseEntity deleteExperience(@PathVariable("id") int id) throws Exception {
        service.deleteExperienceById(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
