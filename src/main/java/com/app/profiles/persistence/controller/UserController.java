package com.app.profiles.persistence.controller;

import com.app.profiles.domain.entity.User;
import com.app.profiles.domain.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService service;

    // GET ALL USERS METHOD
    @GetMapping("/users")
    public ResponseEntity<List<User>> getAll() throws Exception {
        return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    }

    // GET 1 USERS BY ID METHOD
    @GetMapping("/users/{id}")
    public ResponseEntity<User> findById(@PathVariable("id") int id) throws Exception {
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }

    // POST USER METHOD
    @PostMapping("/users")
    public ResponseEntity<User> saveUser(@RequestBody User user) throws Exception {
        return new ResponseEntity<>(service.saveUser(user), HttpStatus.CREATED);
    }

    // PUT USER METHOD
    @PutMapping("/users/{id}")
    public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable("id") int id) throws Exception {
        return new ResponseEntity<>(service.updateUser(user, id), HttpStatus.CREATED);
    }

    // DELETE USER METHOD
    @DeleteMapping("users/{id}")
    public ResponseEntity deleteImage(@PathVariable("id") int id) throws Exception {
        service.deleteUserById(id);
        return new ResponseEntity(HttpStatus.OK);
    }


}
